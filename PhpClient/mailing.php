<!DOCTYPE html>
<html>
<head>
  <title>Gestion des utilisateurs</title>
  <?php require_once'view/head.php'; ?>
</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <?php if(empty($_SESSION['right']) || $_SESSION['right'] > 4) {
    header('Location: http://localhost/PhpClient/view/error.php');
  }?>
  <div class="container">
    <div class="row">
      <h1>MAILING</h1>
    </div>
      <p>Envoyer un mail à tout les abonnés de la news letter</p>
      <?php require_once'view/mailing.html';?>
</div>
    <?php require_once'view/footer.php'; ?>

</body>
</html>
