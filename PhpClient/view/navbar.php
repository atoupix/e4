<?php session_start(); ?>
<nav class="navbar navbar-toggleable-md navbar-light bg-faded">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" id="index" href="index.php">ASM Football</a>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <?php
      if(empty($_SESSION['right'])){
        print('<li class="nav-item">
          <a class="nav-link" id="connexion" href="connexion.php">Connexion</a>
        </li>');
      } else {
        $right = $_SESSION['right'];
        print('<li class="nav-item">
          <a class="nav-link" id="licences" href="licences.php">Gérer les Licenciés</a>
        </li>');
        if($right<2)
        {
          print('
          <li class="nav-item">
            <a class="nav-link" id="utilisateurs" href="utilisateurs.php">Gérer les utilisateurs</a>
          </li>');
        }

      if($right<4)
      {
          print('<li class="nav-item">
            <a class="nav-link" id="mailing" href="mailing.php">Mailing</a>
          </li>');
        }
        print('<li class="nav-item">
          <a class="nav-link" id="disconnected" href="disconnect.php">Deconnexion</a>
        </li>');
      }?>
    </ul>
  </div>
</nav>

<script>

function activeURL(url){
  var curl = document.URL.toString();
  if(curl.indexOf(url)>1){
    $('#'+url).addClass("active");
  }
}
$( document ).ready(function() {
  activeURL("index");
  activeURL("licences");
  activeURL("utilisateurs");
  activeURL("connexion");
});
</script>
