<!DOCTYPE html>
<html>
<head>
  <title>Information</title>
  <?php require_once'view/head.php'; ?>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <?php if($_SESSION['right'] == 3 || $_SESSION['right'] == 5 || empty($_SESSION['right'])) {
    header('Location: http://localhost/PhpClient/view/error.php');
  }?>
  <div class="container">
    <div class="row">
      <h1>Informations Fiancières sur le joueur <spand id="nom_joueur"></span></h1>
    </div>
      <p>Consulter et modifier les informations financières</p>
      <?php require_once'view/finance.html'; ?>
</div>
</body>
<?php require_once'view/footer.php'; ?>
</html>
