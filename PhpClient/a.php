<div class="row">
  <h1>Gestion des licenciés</h1>
</div>
  <p>Consulter, ajouter ou supprimer un licencé</p>
  <div class="row">
  <h2>Gerer les licenciés</h2>
</div>
<select name="saisons" id="saisons"></select>
<button class='btn btn-outline-info btn-sm' id="joueurs"><i aria-hidden='true' class="fa fa-file-pdf-o"></i> Telecharger</button></div>
<!DOCTYPE html>
<html>
<head>
  <title>Gestion des licence</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.3.1/css/buttons.dataTables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>




</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <?php if(empty($_SESSION['right'])){
    header("Location: http://localhost/PhpClient/view/error.php");
  }?>
  <div class="container">
    <div class="row">
      <h1>Gestion des licenciés</h1>
    </div>
    <p>Consulter, ajouter ou supprimer un licencé</p>
    <div class="row">
      <h2>Gerer les licenciés</h2>
    </div>
    <select name="saisons" id="saisons"></select>
    <!--button class='btn btn-outline-info btn-sm' id="joueurs"><i aria-hidden='true' class="fa fa-file-pdf-o"></i> Telecharger</button-->
    <table id="documents_table" class="table table-hover hidden-md-up">
      <thead>
        <tr>
          <th>Détails</th>
          <th>Photo</th>
          <th>Nom</th>
          <th>Prenom</th>
          <th>Mail</th>
          <th>Parents</th>
          <th>Licence</th>
          <?php if($_SESSION['right'] == 1 || $_SESSION['right'] == 2 || $_SESSION['right'] == 4) {
            print("<th>Finance</th>");
          } ?>
          <?php if($_SESSION['right'] < 2 || $_SESSION['right'] == 4) {
            print("<th>Supression</th>");
          }?>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <?php if($_SESSION['right'] == 1 || $_SESSION['right'] == 2 || $_SESSION['right'] == 4) {
            print("<td>sisi</td>");
          }?>
          <?php if($_SESSION['right'] < 2) {
            print("<td>sisi</td>");
          }?>
        </tr>
      </tbody>
    </table>
    <?php require_once'view/getAllJoueur.html'; ?>
    <div class="row">
      <?php if($_SESSION['right'] < 4) {
        print('<a class="btn btn-outline-success" href="new_licence.php" role="button"><i class="fa fa-plus" aria-hidden="true"></i> Nouveau</a>');
      }?>
    </div>
  </div>
</div>
<?php require_once'view/footer.php'; ?>
</body>
</html>
