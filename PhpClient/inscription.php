<!DOCTYPE html>
<html>
<head>
  <title>Inscription</title>
  <?php require_once'view/head.php'; ?>
</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <?php require_once'view/inscription.html'; ?>
  <?php require_once'view/footer.php'; ?>
</body>
</html>
