<!DOCTYPE html>
<html>
<head>
  <title>Gestion des utilisateurs</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>
  <script src="./lib/sweetalert/dist/sweetalert.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./lib/sweetalert/dist/sweetalert.css">

</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <?php if(empty($_SESSION['right']) || $_SESSION['right'] > 2) {
    header('Location: http://localhost/PhpClient/view/error.php');
  }?>
  <div class="container">
    <div class="row">
      <h1>GERER LES UTILISATEURS</h1>
    </div>
      <p>Consulter, ajouter, supprimer ou modifier les utilisateurs</p>
    <?php require_once'view/user.html';?>
</div>
    <?php require_once'view/footer.php'; ?>

</body>
</html>
