<!DOCTYPE html>
<html>
<head>
  <title>Ajouter Licence</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <h1>Accueil</h1>
      <?php
        if(!empty($_SESSION['right'])){
          require_once'view/accueil.html';

        }
        else {
          # code...
          require_once'view/login.html';
        }
      ?>

  </div>
<?php require_once'view/footer.php'; ?>

</body>
</html>
