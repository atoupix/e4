<?php
// CODE ALEX
function getConnection() {
    $dbhost="127.0.0.1";
    $dbuser="root";
    $dbpass="";
    $dbname="bddSISLAM";
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass);
    //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); guys ? wtf is that ????
    return $dbh;
}

function getPrivilege($ndc, $mdp)
{
        $dbh=getConnection();
        $sql = "SELECT id_privilege FROM utilisateurs where password_utilisateur= :pass AND login_utilisateur = :login OR mail_utilisateur = :mail";
        $result=$dbh->prepare($sql);
        $result->execute(array(
          ':pass'=>$mdp,
          ':login'=>$ndc,
          ':mail'=>$ndc
        ));
        return $result->fetchAll(PDO::FETCH_OBJ);
        /*if($result != ""){
          return $result->fetch(PDO::FETCH_OBJ);
        }
        else{
          return "Error";
        }*/
}
/*

function getAllInfoJoueur($id_joueur)
{

    $dbh = getConnection();
    $sql="SELECT id_personne FROM joueur WHERE id_joueur=:id";
    $temp0=$dbh->prepare($sql);
    $temp0->execute(array(
      ':id'=>$id_joueur
    ));
    $data =$temp0->fetchAll(PDO::FETCH_ASSOC);
    //var_dump($data);
    $id_personne=$data["0"]['id_personne'];

    $dbh=null;
    $dbh = getConnection();
    $sql="SELECT * FROM personne WHERE id_personne=:idPersonneJoueur";
    $temp1=$dbh->prepare($sql);
    $temp1->execute(array(
      ':idPersonneJoueur'=>$id_personne
    ));
    $dbh=null;
    $dbh=getConnection();
    $sql="SELECT * FROM joueur WHERE id_personne=:idPersonneJoueur";
    $temp2=$dbh->prepare($sql);
    $temp2->execute(array(
      ':idPersonneJoueur'=>$id_personne
    ));
    $result = array_merge($temp1->fetchAll(PDO::FETCH_OBJ), $temp2->fetchAll(PDO::FETCH_OBJ));
    return $result;

    $dbh=null;
    $result=null;

  }
  */



function addImage($binaire_photo,$name_photo,$filetype,$id)
{
    $bdh = getConnection();
    $result=$bdh->prepare("INSERT INTO `photo`(binaire_photo,name_photo,id_joueur, imagetype) VALUES (:binaire_photo,:name_photo,:id_joueur,:imagetype)");
    $result->execute(array(
      ':binaire_photo'=>$binaire_photo,
      ':name_photo'=>$name_photo,
      ':id_joueur'=>$id,
      'imagetype'=>$filetype
    ));

}
function suppImage($idJoueur){
  $dbh = getConnection();
  $sql = " DELETE FROM photo WHERE id_joueur = :id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$idJoueur
  ));
  echo "image suprimée";
}

//return le blob en base 64
function getB64ImageFromJoueur($idjoueur)
{
  $bdh = getConnection();
  $result=$bdh->prepare("SELECT imageType,binaire_photo FROM photo WHERE id_joueur =".$idjoueur);
  $result->execute();
  $result = $result->fetchAll(PDO::FETCH_ASSOC);
  if(isset($result["0"]['binaire_photo'])){
    $result["0"]['binaire_photo']=base64_encode($result["0"]['binaire_photo']);
  }
  if(ISSET($result[0]))
    return $result[0];
}

function getPaiementComplet($id_joueur)
{
  $dbh=getConnection();
  $result=$dbh->prepare("SELECT l.id_joueur, paiementComplet_payement from etatfinancier e, avoir a, payement p, commun c, licence l WHERE e.id_etatFinancier = a.id_etatFinancier AND a.id_payement = p.id_payement AND p.id_payement = c.id_payement AND c.id_licence = l.id_licence AND l.id_joueur =".$id_joueur);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getEtatFinancier($id_joueur)
{
  $dbh = getConnection();
  $sql="SELECT e.id_etatFinancier, e.sommeAPayer_payement	,e.sommePayer_payement, e.paiementComplet_payement from etatfinancier e, avoir a, payement p, commun c, licence l
      WHERE e.id_etatfinancier = a.id_etatFinancier
      AND a.id_payement = p.id_payement
      AND p.id_payement = c.id_payement
      AND c.id_licence = l.id_licence
      AND l.id_joueur = :id_joueur";
  $result=$dbh->prepare($sql);
  $result->bindParam(':id_joueur', $id_joueur, PDO::PARAM_STR);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
  }

function updateEtatFinancier($idEtatFinancier, $sommeApayer, $sommePayer)
{
  $paiementComplet = null;
  if(intval($sommeApayer) - intval($sommePayer) == 0)
  {
    $paiementComplet = 1;
  } else {
    $paiementComplet = 0;
  }
  $dbh = getConnection();
  $sql = "UPDATE etatfinancier
  SET sommeAPayer_payement=:sommeApayer, sommePayer_payement=:sommePayer, paiementComplet_payement=:paiementComplet
  WHERE id_etatFinancier=:idEtatFinancier";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':sommeApayer'=>$sommeApayer,
    ':sommePayer'=>$sommePayer,
    ':paiementComplet'=>$paiementComplet,
    ':idEtatFinancier'=>$idEtatFinancier
  ));
}

function getLicenceFromJoueur($id_joueur) {
  $dbh = getConnection();
  $sql ='SELECT * FROM licence WHERE id_joueur=:id';
	$resultat=$dbh->prepare($sql);
	$resultat->bindParam(':id', $id_joueur, PDO::PARAM_INT);
	$resultat->execute();
  return $resultat->fetchAll(PDO::FETCH_OBJ);
}

function setLicenceFromJoueur($id_licence,$notes_licence,$date_licence,$cat_licence,$dirigeant_licence,$inscrisNewLetter_licence,$licenceDistrictDispo_licence,$formation_licence,$renouv_licence){
$dbh=getConnection();
$result=$dbh->prepare(" UPDATE licence SET notes_licence=:notes_licence, date_licence=:date_licence, cat_licence=:cat_licence, dirigeant_licence=:dirigeant_licence, inscrisNewLetter_licence=:inscrisNewLetter_licence, licenceDistrictDispo_licence=:licenceDistrictDispo_licence, formation_licence=:formation_licence, renouv_licence=:renouv_licence WHERE id_licence=:id_licence");
$result->execute(array(
      ':notes_licence'=>$notes_licence,
      ':date_licence'=>$date_licence,
      ':cat_licence'=>$cat_licence,
      ':dirigeant_licence'=>$dirigeant_licence,
      ':inscrisNewLetter_licence'=>$inscrisNewLetter_licence,
      ':licenceDistrictDispo_licence'=>$licenceDistrictDispo_licence,
      ':formation_licence'=>$formation_licence,
      ':renouv_licence'=>$renouv_licence,
      ':id_licence'=>$id_licence
    ));
    return "ok !";
  }

function getLastIdFromPersonne()
{
  $dbh = getConnection();
  $sql ='SELECT id_personne FROM personne ORDER BY id_personne DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getLastIdFromPere()
{
  $dbh = getConnection();
  $sql ='SELECT id_pere FROM pere ORDER BY id_pere DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getLastIdFromMere()
{
  $dbh = getConnection();
  $sql ='SELECT id_mere FROM mere ORDER BY id_mere DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function addPersonne($nom_personne, $prenom_personne, $telPortable_personne, $telFixe_personne, $adresse_personne, $ville_personne, $codePostale_personne)
{
  $dbh = getConnection();
  $sql = 'INSERT INTO personne(nom_personne, prenom_personne, telPortable_personne, telFixe_personne, adresse_personne, ville_personne, codePostale_personne)
  VALUES (:nom_personne, :prenom_personne, :telPortable_personne, :telFixe_personne, :adresse_personne, :ville_personne, :codePostale_personne)';
  $result = $dbh->prepare($sql);
  $result->execute(array(
      ':nom_personne'=>$nom_personne,
      ':prenom_personne' => $prenom_personne,
      ':telPortable_personne' => $telPortable_personne,
      ':telFixe_personne' => $telFixe_personne,
      ':adresse_personne' => $adresse_personne,
      ':ville_personne' => $ville_personne,
      ':codePostale_personne' => $codePostale_personne
  ));
}

function addMere()
{
  $temp = getLastIdFromPersonne();
  $idPersonne = null;
  foreach($temp as $id)
  {
    $idPersonne = $id->id_personne;
  }

  $dbh = getConnection();
  $sql = "INSERT INTO mere(id_personne) VALUES(:id_personne)";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':id_personne'=>$idPersonne
  ));
}

function addPere()
{
  $temp = getLastIdFromPersonne();
  $idPersonne = null;
  foreach($temp as $id)
  {
    $idPersonne = $id->id_personne;
  }

  $dbh = getConnection();
  $sql = "INSERT INTO pere(id_personne) VALUES(:id_personne)";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':id_personne'=>$idPersonne
  ));
}


function addJoueur($mail1_joueur, $mail2_joueur)
{
  $idPere = null;
  $idMere = null;
  $idPersonne = null;

  $temp = getLastIdFromPere();
  foreach($temp as $id)
  {
    $idPere = $id->id_pere;
  }
  $temp = getLastIdFromMere();
  foreach($temp as $id)
  {
    $idMere = $id->id_mere;
  }
  $temp = getLastIdFromPersonne();
  foreach($temp as $id)
  {
    $idPersonne = $id->id_personne;
  }
  $dbh = getConnection();
  $sql = 'INSERT INTO joueur(mail1_joueur, mail2_joueur, id_pere, id_mere, id_personne) VALUES(:mail1_joueur, :mail2_joueur, :id_pere, :id_mere, :id_personne)';
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':mail1_joueur'=>$mail1_joueur,
    ':mail2_joueur'=>$mail2_joueur,
    ':id_pere'=>$idPere,
    ':id_mere'=>$idMere,
    ':id_personne'=>$idPersonne
  ));
}

function addEtatPayement($sommeApayer, $sommePayer)
{
  $paiementComplet = null;
  if($sommeApayer-$sommePayer == 0) {
    $paiementComplet = 1;
  } else {
    $paiementComplet = 0;
  }
  $dbh = getConnection();
  $sql="INSERT INTO etatfinancier(sommeAPayer_payement, sommePayer_payement, paiementComplet_payement) VALUES(:sommeApayer, :sommePayer, :paiementComplet)";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':sommeApayer'=>$sommeApayer,
    ':sommePayer'=>$sommePayer,
    ':paiementComplet'=>$paiementComplet
  ));
}

function addPayement($noteFinance, $demandeAttFinancier, $demandeDeCoFinancier, $numChequeFinancier, $depotBanqueFinancier) {
  $dbh = getConnection();
  $sql = "INSERT INTO payement(noteFinanciere_payement, demandeAttFinancier_payement, demandeDeCotiFinancier_payement, numChequeFinancier_payement, depotBanque_payement)
  VALUES(:noteFinance, :demandeAttFinancier, :demandeDeCoFinancier, :numChequeFinancier, :depotBanqueFinancier)";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':noteFinance'=>$noteFinance,
    ':demandeAttFinancier'=>$demandeAttFinancier,
    ':demandeDeCoFinancier'=>$demandeDeCoFinancier,
    ':numChequeFinancier'=>$numChequeFinancier,
    ':depotBanqueFinancier'=>$depotBanqueFinancier
  ));
}

function getLastIdFromJoueur()
{
  $dbh = getConnection();
  $sql ='SELECT id_joueur FROM joueur ORDER BY id_joueur DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getLastIdFromSaison()
{
  $dbh = getConnection();
  $sql ='SELECT id_saison FROM saison ORDER BY id_saison DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function addLicence($notes_licence, $date_licence, $cat_licence, $dirigeant_licence, $inscrisNewLetter_licence, $licenceDistrictDispo_licence, $formation_licence, $renouv_licence) {
  $idJoueur = null;
  $idSaison = null;


  $temp = getLastIdFromJoueur();
  foreach($temp as $id)
  {
    $idJoueur = $id->id_joueur;
  }

  $temp = getLastIdFromSaison();
  foreach($temp as $id)
  {
    $dbh = getConnection();
    $idSaison = $id->id_saison;
  }

  $result=$dbh->prepare("INSERT INTO licence(notes_licence, date_licence, cat_licence, dirigeant_licence, inscrisNewLetter_licence, licenceDistrictDispo_licence,
  formation_licence, renouv_licence, id_joueur, id_saison)
  VALUES(:note_licence, :date_licence, :cat_licence, :dirigeant_licence, :inscrisNewLetter_licence, :licenceDistrictDispo_licence, :formation_licence, :renouv_licence, :id_joueur, :id_saison)");
  $result->execute(array(
        ':note_licence'=>$notes_licence,
        ':date_licence' => $date_licence,
        ':cat_licence' => $cat_licence,
        ':dirigeant_licence' => $dirigeant_licence,
        ':inscrisNewLetter_licence' => $inscrisNewLetter_licence,
        ':licenceDistrictDispo_licence' => $licenceDistrictDispo_licence,
        ':formation_licence' => $formation_licence,
        ':renouv_licence' => $renouv_licence,
        ':id_joueur' => $idJoueur,
        ':id_saison' => $idSaison
  ));
}


function getLastIdFromEtatFinancier()
{
  $dbh = getConnection();
  $sql ='SELECT id_etatFinancier FROM etatfinancier ORDER BY id_etatFinancier DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getLastIdFromPayement()
{
  $dbh = getConnection();
  $sql ='SELECT id_payement FROM payement ORDER BY id_payement DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function addAvoir()
{
  $idEtatFinancier = null;
  $idPayement = null;

  $temp = getLastIdFromEtatFinancier();
  foreach($temp as $id)
  {
    $dbh = getConnection();
    $idEtatFinancier = $id->id_etatFinancier;
  }

  $temp = getLastIdFromPayement();
  foreach($temp as $id)
  {
    $dbh = getConnection();
    $idPayement = $id->id_payement;
  }

  $dbh = getConnection();
  $sql = "INSERT INTO avoir(id_etatFinancier, id_payement) VALUES(:idEtatFinancier, :idPayement)";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':idEtatFinancier'=>$idEtatFinancier,
    ':idPayement'=>$idPayement
  ));
}


function getLastidfromLicence()
{
  $dbh = getConnection();
  $sql ='SELECT id_licence FROM licence ORDER BY id_licence DESC LIMIT 1';
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function addCommun()
{
  $idPayement = null;
  $idLicence = null;

  $temp = getLastIdFromPayement();
  foreach($temp as $id)
  {
    $dbh = getConnection();
    $idPayement = $id->id_payement;
  }

  $temp = getLastidfromLicence();
  foreach($temp as $id)
  {
    $idLicence = $id->id_licence;
  }

  $dbh = getConnection();
  $sql="INSERT INTO commun(id_payement, id_licence) VALUES(:idPayement, :idLicence)";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':idPayement'=>$idPayement,
    ':idLicence'=>$idLicence
  ));
}

function getAllJoueurs($id_saison)
{
  $dbh = getConnection();
  $sql ='SELECT DISTINCT j.*, p.nom_personne, p.prenom_personne FROM joueur j, personne p, licence l, saison s WHERE j.id_personne = p.id_personne AND j.id_joueur=l.id_joueur AND l.id_saison=:idSaison';
	$result =$dbh->prepare($sql);
  $result->execute(array(
    'idSaison'=>$id_saison
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getAllSaisons()
{
  $dbh = getConnection();
  $sql='SELECT * FROM saison';
  $result=$dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getPereFromJoueur($id_pere)
{
  $dbh = getConnection();
  $sql = 'SELECT * FROM pere, personne WHERE id_pere = :id_pere AND pere.id_personne=personne.id_personne';
  $result=$dbh->prepare($sql);
  $result->bindParam(':id_pere', $id_pere, PDO::PARAM_STR);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getMereFromJoueur($id_mere)
{
    $dbh = getConnection();
    $sql = 'SELECT * FROM mere, personne WHERE id_mere = :id_mere AND mere.id_personne=personne.id_personne';
    $result=$dbh->prepare($sql);
    $result->bindParam(':id_mere', $id_mere, PDO::PARAM_STR);
    $result->execute();
    return $result->fetchAll(PDO::FETCH_OBJ);
}



function updatePereAndMere($idPersonnePere, $nomPere, $prenomPere, $telPortablePere, $telFixePere, $addrPere, $villePere, $cpPere, $idPersonneMere, $nomMere, $prenomMere, $telPortableMere, $telFixeMere, $addrMere, $villeMere, $cpMere)
{
  $dbh = getConnection();
  $sql = "UPDATE personne SET nom_personne=:nomPere, prenom_personne=:prenomPere, telPortable_personne=:telPortablePere, telFixe_personne=:telFixePere, adresse_personne=:addrPere, ville_personne=:villePere, codePostale_personne=:cpPere WHERE id_personne=:idPersonnePere";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':nomPere'=>$nomPere,
    ':prenomPere'=>$prenomPere,
    ':telPortablePere'=>$telPortablePere,
    ':telFixePere'=>$telFixePere,
    ':addrPere'=>$addrPere,
    ':villePere'=>$villePere,
    ':cpPere'=>$cpPere,
    ':idPersonnePere'=>$idPersonnePere
  ));

  $result = null;
  $dbh = null;
  $dbh = getConnection();
  $sql = 'UPDATE personne SET nom_personne=:nomMere, prenom_personne=:prenomMere, telPortable_personne=:telPortableMere,
  telFixe_personne=:telFixeMere, adresse_personne=:addrMere, ville_personne=:villeMere,
  codePostale_personne=:cpMere WHERE  id_personne=:idPersonneMere';
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':nomMere'=>$nomMere,
    ':prenomMere'=>$prenomMere,
    ':telPortableMere'=>$telPortableMere,
    ':telFixeMere'=>$telFixeMere,
    ':addrMere'=>$addrMere,
    ':villeMere'=>$villeMere,
    ':cpMere'=>$cpMere,
    ':idPersonneMere'=>$idPersonneMere
  ));
}

function getJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql="SELECT joueur.*, personne.* FROM joueur, personne WHERE joueur.id_joueur = :id AND joueur.id_personne = personne.id_personne;";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':id'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);

}

function updateJoueur($idPersonneJoueur, $nomJoueur, $prenomJoueur, $telPortableJoueur, $telFixeJoueur, $adresseJoueur, $villeJoueur, $cpJoueur, $idJoueur, $mail1Joueur, $mail2Joueur)
{
  $dbh = getConnection();
  $sql="UPDATE personne SET nom_personne=:nomJoueur, prenom_personne=:prenomJoueur, telPortable_personne=:telPortableJoueur, telFixe_personne=:telFixeJoueur, adresse_personne=:adresseJoueur, ville_personne=:villeJoueur, codePostale_personne=:cpJoueur
  WHERE id_personne=:idPersonneJoueur";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':nomJoueur'=>$nomJoueur,
    ':prenomJoueur'=>$prenomJoueur,
    ':telPortableJoueur'=>$telPortableJoueur,
    ':telFixeJoueur'=>$telFixeJoueur,
    ':adresseJoueur'=>$adresseJoueur,
    ':villeJoueur'=>$villeJoueur,
    ':cpJoueur'=>$cpJoueur,
    ':idPersonneJoueur'=>$idPersonneJoueur
  ));

  $dbh = null;
  $dbh=getConnection();
  $sql='UPDATE joueur SET mail1_joueur=:mail1Joueur, mail2_joueur=:mail2Joueur WHERE id_joueur=:idJoueur';
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':mail1Joueur'=>$mail1Joueur,
    ':mail2Joueur'=>$mail2Joueur,
    ':idJoueur'=>$idJoueur
  ));
}

function getIdLicenceFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT licence.id_licence FROM licence where id_joueur=:id_joueur";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPayementFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT p.id_payement FROM payement p , commun c  , licence l WHERE p.id_payement = c.id_payement and c.id_licence = l.id_licence and l.id_joueur = :id_joueur " ;
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdEtatFinancierFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT e.id_etatFinancier from etatfinancier e , avoir a , payement p , commun c , licence l WHERE e.id_etatFinancier = a.id_etatFinancier AND a.id_payement = p.id_payement AND p.id_payement = c.id_payement AND c.id_licence = l.id_licence AND l.id_joueur = :id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdAvoirFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT a.id_avoir FROM avoir a , payement p , commun c , licence l WHERE a.id_payement = p.id_payement AND p.id_payement = c.id_payement AND c.id_licence = l.id_licence AND l.id_joueur = :id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPersonneFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT personne.id_personne FROM personne , joueur WHERE personne.id_personne = joueur.id_personne AND joueur.id_joueur = :id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPereFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT pere.id_pere FROM pere, joueur WHERE pere.id_pere = joueur.id_pere and joueur.id_joueur=:id_joueur" ;
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdMereFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT mere.id_mere FROM mere, joueur WHERE mere.id_mere = joueur.id_mere and joueur.id_joueur=:id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPersonneFromPere($idp)
{
  $dbh = getConnection();
  $sql="SELECT personne.id_personne from personne , pere where personne.id_personne = pere.id_personne AND id_pere = :id_pere ";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_pere'=>$idp
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPersonneFromMere($idMere)
{
  $dbh = getConnection();
  $sql = "SELECT personne.id_personne FROM personne , mere where personne.id_personne = mere.id_personne AND id_mere = :id_mere";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_mere'=>$idMere
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getIdPhotoFromJoueur($id_joueur)
{
  $dbh = getConnection();
  $sql = "SELECT id_photo FROM photo p, joueur j WHERE p.id_joueur = j.id_joueur AND j.id_joueur=:id_joueur";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':id_joueur'=>$id_joueur
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}


function suppLicencier($id_joueur){

  //  $idE = getIdEtatFinancierFromJoueur($id_joueur);
  //  $idPER = getIdPereFromJoueur($id_joueur);
  //  $idM = getIdMereFromJoueur($id_joueur);
  //  $idPERS = getIdPersonneFromJoueur($id_joueur);

  // RECUPARATION DE L'ID DE LA PHOTO DU JOUEUR
  $idphoto = null;
  $temp = getIdPhotoFromJoueur($id_joueur);
  foreach($temp as $photo)
  {
    $idphoto = $photo->id_photo;
  }
  // RECUPERATION DE L'ID DE PERE / MERE / PERSONNE POUR POUVOIR LES SUPPRIMER PAR LA SUITE SANS AVOIR DE SOUCIS
  // PERE
  $idp = null;
  $temp = getIdPereFromJoueur($id_joueur);
  foreach($temp as $pere)
  {
    $idp = $pere->id_pere;
  }

  // MERE
  $idMere = null ;
  $temp = getIdMereFromJoueur($id_joueur);
  foreach($temp as $idM)
  {
    $idMere = $idM->id_mere;
  }

  // PERSONNE PERE
  $idPersonnePere = null;
  $temp = getIdPersonneFromPere($idp);
  foreach($temp as $IdPersonnePere)
  {
    $idPersonnePere = $IdPersonnePere->id_personne;
  }

  // PERSONNE MERE
  $idPersonneMere = null;
  $temp = getIdPersonneFromMere($idMere);
  foreach($temp as $IdPersonneMere)
  {
    $idPersonneMere = $IdPersonneMere->id_personne;
  }


  // PERSONNE
  $idPers = null ;
  $temp = getIdPersonneFromJoueur($id_joueur);
  foreach($temp as $idP)
  {
    $idPers = $idP->id_personne;
  }


  // SUPRESSION DE LA PHOTO

    $dbh = getConnection();
    $sql = " DELETE FROM photo WHERE id_photo = :id_photo";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      ':id_photo'=>$idphoto
    ));

  // SUPRESSION DE AVOIR

    $idetatFinancier = null ;
    $idPayement = null;
    $temp = getIdEtatFinancierFromJoueur($id_joueur);
    foreach($temp as $idEtat)
    {
        $idetatFinancier = $idEtat->id_etatFinancier;
    }

    $temp = getIdPayementFromJoueur($id_joueur);
    foreach($temp as $payement)
    {
      $idPayement = $payement->id_payement;
    }

    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM avoir where id_etatFinancier =:id_etatFinancier AND id_payement =:id_payement";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_payement',$idPayement,PDO::PARAM_STR);
    $result->bindParam('id_etatFinancier',$idetatFinancier,PDO::PARAM_STR);
    $result->execute();


    // SUPRESSION DE COMMUN
    $result = null;
    $idLicence = null;
    $dbh = null ;
    $temp = getIdLicenceFromJoueur($id_joueur);
    foreach($temp as $licence)
    {
      $idLicence = $licence->id_licence;
    }

    $dbh = getConnection();
    $sql = "DELETE FROM commun where id_payement =:id_payement and id_licence=:id_licence";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_payement',$idPayement,PDO::PARAM_STR);
    $result->bindParam('id_licence',$idLicence ,PDO::PARAM_STR);
    $result->execute();


    // SUPRESSION DE PAYEMENT
    $result = null ;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM payement where id_payement =:id_payement";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_payement',$idPayement,PDO::PARAM_STR);
    $result->execute();

    // SUPRESSION D'ETAT FINANCIER
    $result = null;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM etatfinancier where id_etatFinancier = :id_etatFinancier";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_etatFinancier',$idetatFinancier,PDO::PARAM_STR);
    $result->execute();

    // SUPRESSION DE LICENCE
    $result = null ;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM licence where id_joueur = :id_joueur";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_joueur',$id_joueur,PDO::PARAM_STR);
    $result->execute();

    // SUPRESSION DE JOUEUR
    $result = null ;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM joueur where id_joueur = :id_joueur";
    $result = $dbh->prepare($sql);
    $result->bindParam('id_joueur',$id_joueur);
    $result->execute();

    //SUPRESSION DE PERSONNE JOUEUR
    $result = null;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM personne where id_personne=:id_personne";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      ':id_personne'=>$idPers
    ));

    // SUPRESSION DE LA MERE
    $result = null ;
    $dbh = null ;
    $dbh = getConnection();
    $sql = "DELETE FROM mere where id_mere=:id_mere";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      ':id_mere'=>$idMere
    ));

    // SUPRESSION DE PERSONNE MERE
    $result = null;
    $dbh = null;
    $dbh=getConnection();
    $sql = "DELETE FROM personne where id_personne = :id_personne";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      ':id_personne'=>$idPersonneMere
    ));

    /// SUPRESSION DU PERE
    $result= null;
    $dbh = null;
    $dbh = getConnection();
    $sql = "DELETE FROM pere where id_pere = :id_pere";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      ':id_pere'=>$idp
    ));

    // SUPRESSION DE PERSONNE PERE
    $result = null;
    $dbh = null;
    $dbh=getConnection();
    $sql = "DELETE FROM personne where id_personne = :id_personne";
    $result = $dbh->prepare($sql);
    $result->execute(array(
      'id_personne'=>$idPersonnePere
    ));

}


function getPayment($id_joueur)

{
    $dbh=getConnection();
    $sql="SELECT payement.* FROM licence, commun, payement WHERE licence.id_joueur=:idJoueur AND licence.id_licence=commun.id_licence AND commun.id_payement = payement.id_payement";
    $result=$dbh->prepare($sql);
    $result->execute(array(
      ':idJoueur'=>$id_joueur
    ));
    return $result->fetchAll(PDO::FETCH_OBJ);
}


function updatePayement($idFinance, $noteFinance, $demandeAttFinance, $demandeCotiFinance, $numChequeFinance, $depotBanqueFinance)
{
  $dbh = getConnection();
  $sql="UPDATE payement SET noteFinanciere_payement=:noteFinance,
  demandeAttFinancier_payement=:demandeAttFinance, demandeDeCotiFinancier_payement=:demandeCotiFinance,
  numChequeFinancier_payement=:numChequeFinance, depotBanque_payement=:depotBanqueFinance
  WHERE id_payement=:idFinance";
  $result=$dbh->prepare($sql);
  $result->execute(array(
    ':noteFinance'=>$noteFinance,
    ':demandeAttFinance'=>$demandeAttFinance,
    ':demandeCotiFinance'=>$demandeCotiFinance,
    ':numChequeFinance'=>$numChequeFinance,
    ':depotBanqueFinance'=>$depotBanqueFinance,
    ':idFinance'=>$idFinance
  ));
}

function getAllUser()
{
  $dbh = getConnection();
  $sql="SELECT * FROM utilisateurs, privilege WHERE utilisateurs.id_privilege = privilege.id_privilege";
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getAllPriv()
{
  $dbh = getConnection();
  $sql = "SELECT * FROM privilege";
  $result = $dbh->prepare($sql);
  $result->execute();
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function getUser($idUser)
{
  $dbh = getConnection();
  $sql = "SELECT * FROM utilisateurs WHERE id_utilisateur = :idUser";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':idUser'=>$idUser
  ));
  return $result->fetchAll(PDO::FETCH_OBJ);
}

function updateUser($idUser, $login, $mail, $password, $catPriv)
{
  $dbh = getConnection();
  $sql = "UPDATE utilisateurs SET login_utilisateur=:login, password_utilisateur=:password, mail_utilisateur=:mail, id_privilege=:idPriv WHERE id_utilisateur=:idUser";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':login'=>$login,
    ':password'=>$password,
    ':mail'=>$mail,
    ':idPriv'=>$catPriv,
    ':idUser'=>$idUser
  ));

  return "Modifié !";
}

function addUser($login, $mail, $password, $catPriv)
{
    $dbh = getConnection();
    $sql = "INSERT INTO utilisateurs(login_utilisateur, password_utilisateur, mail_utilisateur, id_privilege) VALUES (:login, :password, :mail, :idPriv)";
    $result = $dbh->prepare($sql);
    $result->execute(array(
    ':login'=>$login,
    ':password'=>$password,
    ':mail'=>$mail,
    ':idPriv'=>$catPriv
  ));

  return "Ajouté !";
}

function deleteUser($idUser)
{
  $dbh = getConnection();
  $sql = "DELETE FROM utilisateurs where id_utilisateur=:idUser";
  $result = $dbh->prepare($sql);
  $result->execute(array(
    ':idUser'=>$idUser
  ));
  return "Supprimer !";
}
