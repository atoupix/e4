<?php
require 'slim3/vendor/autoload.php';
require '../model/model.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$configuration = [
  'settings' => [
    'displayErrorDetails' => true,
  ],
];
$c = new \Slim\Container($configuration);
$app = new \Slim\App($c);

$app->post('/user/connection', function($request, $response, $args) {
  $user = json_decode($request->getBody());
  $result = getPrivilege($user->login, $user->password);
  $idPriv = null;
  foreach($result as $id)
  {
    $idPriv = $id->id_privilege;
  }
  if($idPriv == "")
  {
    echo "Error";
  } else {
    echo $idPriv;
  }
});

$app->get('/users', function($request, $response, $args) {
  $users = getAllUser();
  echo json_encode($users, JSON_PRETTY_PRINT);
});
$app->get('/user/{id}', function($request, $response, $args) {
  $user = getUser($args['id']);
  echo json_encode($user, JSON_PRETTY_PRINT);
});
//FAUX
//$app->post('/user/add', function($request, $response, $args) {
$app->post('/user', function($request, $response, $args) {
  $user = json_decode($request->getBody());
  $result = addUser($user->login, $user->mail, $user->password, $user->catPriv);
  echo $result;
});
//FAUX
//$app->post('/user/update', function($request, $response, $args) {
$app->put('/user/{id}', function($request, $response, $args) {
  $user = json_decode($request->getBody());
  $return = updateUser($user->idUser, $user->login, $user->mail, $user->password, $user->catPriv);
  echo $return;
});
//FAUX
//$app->post('/user/delete/{id}', function($request, $response, $args) {
$app->delete('/user/{id}', function($request, $response, $args) {
  $result = deleteUser($args['id']);
  echo $result;
});

$app->get('/users/privs', function($request, $response, $args) {
  $priv = getAllPriv();
  echo json_encode($priv, JSON_PRETTY_PRINT);
});


$app->get('/joueurs/{idSaison}', function($request, $response, $args) {
  $result = getAllJoueurs($args['idSaison']);
  echo json_encode($result, JSON_PRETTY_PRINT);//idSaison
});
//Avoir un joueur, c'est pas avoir ses infos justement ?
//Dans tout les cas c'est joueur/id/info donc :
//FAUX
//$app->get('/joueur/info/{id}', function($request, $response, $args) {
$app->get('/joueur/{id}', function($request, $response, $args) {
  $result = getJoueur($args['id']);
  echo json_encode($result, JSON_PRETTY_PRINT);
});
//$app->get('/joueur/licence/{id}', function ($request, $response, $args) {
$app->get('/joueur/{id}/licence', function ($request, $response, $args) {
  //TODO
  $result = getLicenceFromJoueur($args['id']);
  echo json_encode($result, JSON_PRETTY_PRINT);
});
//Bizarre
//$app->get('/finance/get/{idJoueur}', function($request, $response, $args) {
$app->get('/joueur/{idJoueur}/finance', function($request, $response, $args) {
  $result = getPayment($args['idJoueur']);
  echo "paye1".json_encode($result, JSON_PRETTY_PRINT);
  $result2 = getEtatFinancier($args['idJoueur']);
  echo "etat1".json_encode($result2,JSON_PRETTY_PRINT);
});

//la route pour avoir l'état financier booleein d'un joueur (utilisé pour le changement du couleur du bouton)
//$app->get('/{idJoueur}/paiement_complet', function($request, $response, $args) {
$app->get('/joueur/{idJoueur}/paiement_complet', function($request, $response, $args) {
  $result = getPaiementComplet($args['idJoueur']);
  echo json_encode($result, JSON_PRETTY_PRINT);
});

//$app->get('/{idJoueur}/img', function($request, $response, $args) {
$app->get('/joueur/{idJoueur}/img', function($request, $response, $args) {
  $result = getB64ImageFromJoueur($args['idJoueur']);
  echo json_encode($result, JSON_PRETTY_PRINT);
});

$app->post('/joueur', function($request, $response, $args) {
  $licencier = json_decode($request->getBody());
  addPersonne($licencier->nomMere, $licencier->prenomMere, $licencier->telPortableMere, $licencier->telFixeMere, $licencier->adresseMere, $licencier->villeMere, $licencier->cpMere);
  addMere();
  addPersonne($licencier->nomPere, $licencier->prenomPere, $licencier->telPortablePere, $licencier->telFixePere, $licencier->adressePere, $licencier->villePere, $licencier->cpPere);
  addPere();
  addPersonne($licencier->nomJoueur, $licencier->prenomJoueur, $licencier->telPortableJoueur, $licencier->telFixeJoueur, $licencier->adresseJoueur, $licencier->villeJoueur, $licencier->cpJoueur);
  addEtatPayement($licencier->sommeApayer, $licencier->sommePayer);
  addJoueur($licencier->mail1Joueur, $licencier->mail2Joueur);
  addPayement($licencier->noteFinance, $licencier->demandeAttFinancier, $licencier->demandeDeCoFinancier, $licencier->numChequeFinancier, $licencier->depotBanqueFinancier);
  addLicence($licencier->notesLicence, $licencier->dateLicence, $licencier->catLicence, $licencier->dirigeanLicence, $licencier->newLetterLicence, $licencier->licenceDisctrictDispoLicence, $licencier->formationlicence, $licencier->renouvLicence);
  addAvoir();
  addCommun();
  echo "ok";
});

//Bon ca c'est moi, aygalic qui l'ai fait, et j'ai rien compris non plus c'est marrant
//$app->post('/{idJoueur}/img', function($request, $response, $args) {
$app->post('/joueur/{idJoueur}/img', function($request, $response, $args) {
  $sourcePath = $_FILES['file']['tmp_name'];
  $imagedata = file_get_contents($sourcePath);
  $filename = $_FILES['file']['name'];
  $filetype = $_FILES["file"]["type"];
  addImage($imagedata,$filename,$filetype,$args['idJoueur']);
  echo "Reçue par le serveur";
});
//Pareil, bizarre
//$app->post('/finance/modif', function($request, $response, $args) {
$app->put('/joueur/{id}/finance', function($request, $response, $args) {
  $finance = json_decode($request->getBody());
  updatePayement($finance->idFinance, $finance->noteFinance, $finance->demandeAttFinance, $finance->demandeCotiFinance, $finance->numChequeFinance, $finance->depotBanqueFinance);
  updateEtatFinancier($finance->idEtatFinance, $finance->sommeApayer, $finance->sommePayer);
});
//FAUX
//$app->post('/licence/modif', function ($request, $response, $args) {
$app->put('/joueur/{id}/licence', function ($request, $response, $args) {
  $licence = json_decode($request->getBody());
  $test = setLicenceFromJoueur($licence->idlicence,$licence->notesLicence,$licence->datelicence,$licence->catLicence,$licence->dirigeantLicence,$licence->inscrisNewLetterLicence,$licence->licenceDisctrictDispoLicence,$licence->formationlicence,$licence->renouvLicence);
  echo $test;
});
//FAUX
//$app->post('/joueur/modif', function($request, $response, $args) {
$app->put('/joueur/{id}', function($request, $response, $args) {
  $joueur = json_decode($request->getBody());
  updateJoueur($joueur->idPersonneJoueur, $joueur->nomJoueur, $joueur->prenomJoueur, $joueur->telPortableJoueur, $joueur->telFixeJoueur, $joueur->adresseJoueur, $joueur->villeJoueur, $joueur->cpJoueur, $joueur->idJoueur, $joueur->mail1Joueur, $joueur->mail2Joueur);
  echo "modifié !";
});
//FAUX
//$app->post('/licencier/supp/{idJoueur}',function($request,$response,$args){
$app->delete('/joueur/{idJoueur}',function($request,$response,$args){
  $test = suppLicencier($args['idJoueur']);
  echo " Utilisateur Supprimé " ;
});
$app->delete('/joueur/{idJoueur}/img', function($request, $response, $args) {
  suppImage($args['idJoueur']);
});

$app->get('/saisons', function($request, $response, $args) {
  $result = getAllSaisons();
  echo json_encode($result, JSON_PRETTY_PRINT);
});


$app->get('/parents/{ids}', function($request, $response, $args) {
  $result = $args['ids'];
  $ids = explode(" ", $result);
  if(sizeOf($ids) == 0)
  {
    echo "Error !";
  } elseif (sizeOf($ids) == 1) {
    echo "Nice try !";
  } else {
    $pere = getPereFromJoueur($ids[0]);
    $mere = getMereFromJoueur($ids[1]);
    echo "pere".json_encode($pere, JSON_PRETTY_PRINT);
    echo "mere".json_encode($mere, JSON_PRETTY_PRINT);
  }
});
//FAUX
//$app->post('/parents/modif', function($request, $response, $args) {
$app->put('/parents', function($request, $response, $args) {
  $parents = json_decode($request->getBody());
  updatePereAndMere($parents->idPersonnePere, $parents->nomPere, $parents->prenomPere,$parents->telPortablePere, $parents->telFixePere, $parents->addrPere,$parents->villePere, $parents->codePostalePere,$parents->idPersonneMere, $parents->nomMere, $parents->prenomMere,$parents->telPortableMere, $parents->telFixeMere, $parents->addrMere,$parents->villeMere, $parents->codePostaleMere);
});







/*
A faire plus tard
reponse d'initialisation du saison
Exploitation des données :
reponse d'affichage Etat des paiements: Nom, prénom, montant attendu, montant payé, paiement complet
reponse d'affichage Annuaire simple: Nom, prénom, photo, numéro de licence, téléphone portable de contact
reponse d'affichage Annuaire complet: annuaire simple + coordonnées des parents, adresse mail*/

$app->run();
