<!DOCTYPE html>
<html>
<head>
  <title>Ajouter Licence</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <div class="row">
      <h1>AS FOOTBALL</h1>
    </div>
    <p>bienvenue sur le site de gestion de l'association de football</p>
    <div class="row">
      <?php
        if(!empty($_SESSION['right'])){
          echo '<div class="col-md-3">
            <h2>Navigation</h2>
            <ul class="nav flex-column">
              <li class="nav-item ">
                <a class="nav-link" href="index.php">Accueil <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="licences.php">Gérer les Licenciés</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="utilisateurs.php">Gérer les utilisateurs</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="connexion.php">Connexion</a>
              </li>
            </ul>
          </div>';
        }
      ?>
      <div class="col-md-6">
        <?php require_once'view/login.html'; ?>
      </div>
    </div>

  </div>
</div>
<?php require_once'view/footer.php'; ?>

</body>
</html>
