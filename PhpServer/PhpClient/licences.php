<!DOCTYPE html>
<html>
<head>
  <title>Gestion des licence</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <div class="row">
      <h1>Gestion des licenciés</h1>
    </div>
      <p>Consulter, ajouter ou supprimer un licencé</p>
      <div class="row">
      <h2>Gerer les licenciés</h2>
    </div>
      <?php require_once'view/getAllJoueur.html'; ?>
      <div class="row">
        <a class="btn btn-outline-success" href="new_licence.php" role="button">Ajouter une licence</a>
    </div>
</div>
    <?php require_once'view/footer.php'; ?>

</body>
</html>
