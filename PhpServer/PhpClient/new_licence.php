<!DOCTYPE html>
<html>
<head>
  <title>Ajout d'une licence</title>
  <?php require_once'view/head.php'; ?>
</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <div class="row">
      <h1>AS FOOTBALL</h1>
    </div>
    <div class="row">
      <h2>Ajouter un licencié</h2>
    </div>
      <?php require_once'view/AddLicence.html'; ?>

    </div>
</div>
    <?php require_once'view/footer.php'; ?>

</body>
</html>
