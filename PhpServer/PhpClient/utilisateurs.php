<!DOCTYPE html>
<html>
<head>
  <title>Gestion des utilisateurs</title>
  <?php require_once'view/head.php'; ?>
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.css"/>
  <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.13/datatables.min.js"></script>

</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <div class="row">
      <h1>AS FOOTBALL</h1>
    </div>
      <p>bienvenue sur le site de gestion de l'association de football</p>
    <?php require_once'view/user.html';?>
</div>
    <?php require_once'view/footer.php'; ?>

</body>
</html>
