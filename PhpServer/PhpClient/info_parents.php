<!DOCTYPE html>
<html>
<head>
  <title>Information</title>
  <?php require_once'view/head.php'; ?>
</head>
<body>
  <?php require_once'view/navbar.php'; ?>
  <div class="container">
    <div class="row">
      <h1>Information sur le joueur <spand id="nom_joueur"></span></h1>
    </div>
      <p>Consulter et modifier les informations sur les parents d'un joueur</p>
      <?php require_once'view/parents.html'; ?>
</div>
</body>
<?php require_once'view/footer.php'; ?>
</html>
